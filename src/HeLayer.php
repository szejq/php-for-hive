<?php

declare(strict_types=1);
#test edit

namespace Hive\PhpLib;

/**
 * Hive-Engine transport layer
 *
 * Hive Toolbox PHP transport class
 * The HeLayer class contains all methods needed to send queries
 * and receive response from HiveEngine sidechain.
 *
 * @category Layers
 * @package  HiveEngine
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class HeLayer
{
    /**
     * If true, display the query & the result. Default: false
     *
     * @var bool $debug
     */
    private bool $debug = false;

    /**
     * Default node for query HiveEngine API
     *
     * @var string $heNode
     */
    private string $heNode = "api.hive-engine.com/rpc";

    /**
     * If turned to true, curl disable SSL verification
     *
     * @var bool $disableSsl
     */
    private bool $disableSsl = false;

    /**
     * If true, Throw exception to stop execution
     *
     * @var bool $throwExceptions
     */
    private bool $throwExceptions = false;

    /**
     * Use HTTPS scheme by default
     *
     * @var string $scheme
     */
    private string $scheme = 'https://';

    /**
     * Constructor to use Hive and HiveEngine API with config
     * If $config is not present, use default values
     *
     * @param array<bool|string> $config Configuration array
     *
     * @return void
     */
    public function __construct(array $config = array())
    {

        if (array_key_exists('debug', $config)) {
            $this->debug = (bool) $config['debug'];
        }

        if (array_key_exists('heNode', $config)) {
            $this->heNode = (string) $config['heNode'];
        }

        if (array_key_exists('disableSsl', $config)) {
            $this->disableSsl = (bool) $config['disableSsl'];
        }

        if (array_key_exists('throwExceptions', $config)) {
            $this->throwExceptions = (bool) $config['throwExceptions'];
        }
    }

    /**
     * Function to generate JSON object for (RPC) request,
     * execute the query with curl(), and return the results
     *
     * @param string $method Method
     * @param array  $params Array of parameters
     *
     * @return array $response JSON object from HiveEngine response
     */
    public function call(string $method, array $params = array()): array
    {
        if (!empty($params['contract'])) {
            $endpoint = "/contracts";
            $contract = (string) $params['contract'];
            $table = (string) $params['table'];
            $query = (array) $params['query'];
            $limit = (int) $params['limit'];
            $request = $this->getContractRequest($method, $contract, $table, $query, $limit);
        } else {
            $endpoint = "/blockchain";
            $request = $this->getRPCRequest($method, $params);
        }
        $json_response = $this->curl($request, $endpoint);

        $response = (array) json_decode($json_response, true);

        if (!array_key_exists('result', $response)) {
            if ($this->throwExceptions) {
                throw new \Exception('Error retrieve HiveEngine API');
            }
            return $response;
        }

        return (array) $response['result'];
    }

    /**
     * Function to get Hive Engine history for an account or a token
     *
     * @param string  $account Hive account
     * @param string  $token   Hive Engine token name
     * @param integer $limit   Maximum number of result
     *
     * @return array $response Array from HiveEngine JSON object response
     */
    public function callHistory(string $account = "", string $token = "", $limit = 100): array
    {
        $heHistory = "https://accounts.hive-engine.com/accountHistory";
        $historyUrl = $heHistory . "?account=" . $account . "&symbol=" . $token . "&limit=" . $limit;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $historyUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($this->disableSsl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        $result = strval(curl_exec($ch));

        if ($this->debug) {
            echo "<pre><br>Result :<br>" . $result . "</pre>\n";
        }

        $response = (array) json_decode($result, true);

        if (!array_key_exists('result', $response)) {
            if ($this->throwExceptions) {
                throw new \Exception('Error retrieve HiveEngine History API');
            }
            return $response;
        }

        return (array) $response['result'];
    }

    /**
     * Execute the cURL query and return the response JSON object
     *
     * @param string $data     JSON object send to Hive Engine selected node
     * @param string $endpoint (optional) Selected endpoint to execute the query
     *
     * @return string $result result JSON object
     */
    public function curl(string $data, string $endpoint = ''): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->scheme . $this->heNode . $endpoint);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        if ($this->disableSsl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        $result = strval(curl_exec($ch));

        if ($this->debug) {
            echo "<pre><br>Result :<br>" . $result . "</pre>\n";
        }

        return $result;
    }

    /**
     * Function to generate JSON response for RPC HIVE & HE request from query
     *
     * @param string $method Method
     * @param array  $params Array of parameters
     *
     * @return string $response JSON object from HiveEngine response
     */
    public function getRPCRequest(string $method, array $params = array()): string
    {
        if (empty($params)) {
            $request = array(
                "jsonrpc" => "2.0",
                "method" => $method,
                "id" => 0
            );
        } else {
            $request = array(
                "jsonrpc" => "2.0",
                "method" => $method,
                "params" => $params,
                "id" => 0
            );
        }

        $request_json = json_encode($request);

        if ($this->debug) {
            echo "<pre><br>request_json<br/>";
            print $request_json . "\n";
            echo "</pre>";
        }
        return $request_json;
    }

    /**
     * Create the JSON object for normal request
     *
     * @param string  $method   Method to get data : find / find_one / get_contract, ...
     * @param string  $contract Name of the selected contract
     * @param string  $table    Name of the table
     * @param array  $query    Executed query
     * @param integer $limit    Maximum number of results
     *
     * @return string $request_json JSON object ready to be send by curl() function
     */
    public function getContractRequest(
        string $method,
        string $contract,
        string $table,
        array $query,
        int $limit = 1
    ): string {
        $request = array(
            "id" => 1,
            "jsonrpc" => "2.0",
            "method" => $method,
            "params" => array(
                "contract" => $contract,
                "table" => $table,
                "query" => $query,
            ),
            "limit" => $limit
        );

        $request_json = json_encode($request);

        if ($this->debug) {
            echo "<pre>request_json<br/>" . $request_json . "\n</pre>";
        }

        return $request_json;
    }
}
