<?php

declare(strict_types=1);

namespace Hive\PhpLib\HiveEngine;

use Hive\PhpLib\HeLayer as HeLayer;

/**
 * Class Blockchain
 *
 * @category Methods
 * @package  HiveEngine
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class Blockchain
{
    /**
     * Needed layer for Hive Engine communication
     */
    private object $HeLayer;

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HeLayer = new HeLayer($config);
    }

    /**
     * Get the status of the selected HiveEngine node
     *
     * @return array $result State of selected node
     **/
    public function getStatus(): array
    {
        $params = array();
        $result = $this->HeLayer->call('getStatus', $params);
        return $result;
    }

    /**
     * Get the Latest block data : timestamp, txs, hash, ...
     *
     * @return array $result Last block data
     **/
    public function getLatestBlockInfo(): array
    {
        $params = array();
        $result = $this->HeLayer->call('getLatestBlockInfo', $params);
        return $result;
    }

    /**
     * Get selected block data : timestamp, txs, hash, ...
     *
     * @param string $block selected block ID
     *
     * @return array $result selected block data
     **/
    public function getBlockInfo(string $block): array
    {
        $params = array(
            "blockNumber" => $block
        );
        $result = $this->HeLayer->call('getBlockInfo', $params);
        return $result;
    }

    /**
     * Get selected tx data : timestamp, hash, amount, ...
     *
     * @param string $txid selected block ID
     *
     * @return array selected TX data
     **/
    public function getTransactionInfo(string $txid): array
    {
        $params = array(
            "txid" => $txid
        );
        $result = $this->HeLayer->call('getTransactionInfo', $params);
        return $result;
    }
}
