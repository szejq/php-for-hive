<?php

declare(strict_types=1);

namespace Hive\PhpLib\HiveEngine;

use Hive\PhpLib\HeLayer as HeLayer;

/**
 * Tokens related methods
 *
 * @category Methods
 * @package  HiveEngine
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 **/
class Tokens
{
    /**
     * Needed layer for Hive Engine communication
     */
    private object $HeLayer;

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HeLayer = new HeLayer($config);
    }

    /**
    * Balances of the users
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result Balances from selected account
    **/
    public function balances(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "tokens",
            "table" => "balances",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );

        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Opened delegations
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result List of opened delegations
    **/
    public function delegations(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "tokens",
            "table" => "delegations",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );

        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Pending unstakes
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result list of pending unstakes
    **/
    public function pendingUnstakes(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "tokens",
            "table" => "pendingUnstakes",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );

        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Pending undelegations
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result list of pending undelegations
    **/
    public function pendingUndelegations(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "tokens",
            "table" => "pendingUndelegations",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );

        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Contracts balances
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result list of contracts balances
    **/
    public function contractsBalances(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "tokens",
            "table" => "contractsBalances",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );

        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Contract parameters
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result list of contracts balances
    **/
    public function params(): array
    {
        $params = array(
            "contract" => "tokens",
            "table" => "params",
            "query" => array()
        );

        $result = $this->HeLayer->call('findOne', $params);
        return $result;
    }
}
