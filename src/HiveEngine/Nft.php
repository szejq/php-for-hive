<?php

declare(strict_types=1);

namespace Hive\PhpLib\HiveEngine;

use Hive\PhpLib\HeLayer as HeLayer;

/**
 * NFT related methods
 *
 * @category Methods
 * @package  HiveEngine
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 **/
class Nft
{
    /**
     * Needed layer for Hive Engine communication
     */
    private object $HeLayer;

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HeLayer = new HeLayer($config);
    }

    /**
    * Contains contract parameters such as the current fees
    *
    * @return array $result Contract parameters
    **/
    public function params(): array
    {
        $params = array(
            "contract" => "nft",
            "table" => "params",
            "query" => array(),
        );
        $result = $this->HeLayer->call('findOne', $params);
        return $result;
    }

    /**
    * Get Definitions of each NFT
    *
    * @param string $symbol Select token symbol
    *
    * @return array $result NFT definitions
    **/
    public function nfts(string $symbol): array
    {
        $params = array(
            "contract" => "nft",
            "table" => "nfts",
            "query" => array(
                "symbol" => $symbol,
            )
        );
        $result = $this->HeLayer->call('findOne', $params);
        return $result;
    }

    /**
    * Contains records of all undelegations which are waiting for cooldown to complete
    *
    * @param string $symbol Select token symbol
    *
    * @return array $result Cooldowns of undelegations
    **/
    public function pendingUndelegations(string $symbol): array
    {
        $params = array(
            "contract" => "nft",
            "table" => "pendingUndelegations",
            "query" => array(
                "symbol" => $symbol,
            )
        );
        $result = $this->HeLayer->call('find', $params);
        return $result;
    }

    /**
    * Contains records of all undelegations which are waiting for cooldown to complete
    *
    * @param string $symbol Select token symbol
    * @param string $ownedBy "u" for Hive user, "c" for contract
    * @param string $account Selected HIVE account
    *
    * @return array $result Cooldowns of undelegations
    **/
    public function symbolInstances(string $symbol, string $ownedBy, string $account): array
    {
        $table = $symbol . "instances";
        $params = array(
            "contract" => "nft",
            "table" => $table,
            "query" => array(
                "ownedBy" => $ownedBy,
                "account" => $account
            )
        );
        $result = $this->HeLayer->call('find', $params);
        return $result;
    }
}
