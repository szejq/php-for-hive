<?php

declare(strict_types=1);

namespace Hive\PhpLib\HiveEngine;

use Hive\PhpLib\HeLayer as HeLayer;

/**
 * Market related methods
 *
 * @category Methods
 * @package  HiveEngine
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 **/
class Market
{
    /**
     * Needed layer for Hive Engine communication
     */
    private object $HeLayer;

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HeLayer = new HeLayer($config);
    }

    /**
    * Get list of all the pending buy orders
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result Pending buy orders
    **/
    public function buyBook(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "market",
            "table" => "buyBook",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );
        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Get list of all the pending sell orders
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result Pending sell orders
    **/
    public function sellBook(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "market",
            "table" => "sellBook",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );
        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Get list of trading txs
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result Trades history TXs list
    **/
    public function tradesHistory(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }

        $params = array(
            "contract" => "market",
            "table" => "tradesHistory",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );
        $result = $this->HeLayer->call($method, $params);
        return $result;
    }

    /**
    * Get metrics from token (and account if completed)
    *
    * @param string $symbol Select token symbol
    * @param string $account Selected HIVE account
    *
    * @return array $result Metrics from the selected token
    **/
    public function metrics(string $symbol, string $account = ""): array
    {
        if ($account !== "") {
            $method = "findOne";
        } else {
            $method = "find";
        }
        $params = array(
            "contract" => "market",
            "table" => "tradesHistory",
            "query" => array(
                "symbol" => $symbol,
                "account" => $account
            )
        );
        $result = $this->HeLayer->call($method, $params);
        return $result;
    }
}
