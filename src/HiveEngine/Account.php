<?php

declare(strict_types=1);

namespace Hive\PhpLib\HiveEngine;

use Hive\PhpLib\HeLayer as HeLayer;

/**
 * Account related methods
 *
 * @category Methods
 * @package  HiveEngine
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 **/
class Account
{
    /**
     * Needed layer for Hive Engine communication
     */
    private object $HeLayer;

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HeLayer = new HeLayer($config);
    }

    /**
    * Get selected account Hive Engine tokens balance
    *
    * @param string $account Selected HIVE account
    *
    * @return array $result Balances from selected account
    **/
    public function getAccountBalance($account): array
    {
        $params = array(
            "contract" => "tokens",
            "table" => "balances",
            "query" => array(
                "account" => $account
            ),
            "limit" => 1
        );
        $result = $this->HeLayer->call('find', $params);
        return $result;
    }

    /**
    * Get selected account Hive Engine history
    *
    * @param string $account Selected HIVE account
    * @param string $token Optional : Get history only for selected tokens
    * @param int $limit Max number of results
    *
    * @return array $result Account history
    **/
    public function getAccountHistory($account, $token = null, $limit = 100): array
    {
        if ($token !== null) {
            $token = strtoupper($token);
        }

        $result = $this->HeLayer->callHistory($account, $token, $limit);
        return $result;
    }

    /**
    * Get pending undelegations from selected account. Optional : only one token
    *
    * @param string $account Selected HIVE account
    * @param string $token Optional : filter with only selected token
    * @param int $limit Optional : Max number of results
    *
    * @return array $result response
    **/
    public function getPendingUndelegations(string $account, string $token = null, int $limit = 1): array
    {
        if ($token !== null) {
            $query = array(
                "account" => $account,
                "symbol" => strtoupper($token)
            );
        } else {
            $query = array(
                "account" => $account
            );
        }
        $params = array(
            "contract" => "tokens",
            "table" => "pendingUndelegations",
            "query" => $query,
            "limit" => $limit
        );

        $result = $this->HeLayer->call('find', $params);
        return $result;
    }

    /**
    * Get all delegations to selected account. Optional : only one token
    *
    * @param string $account Selected HIVE account
    * @param string $token Optional : filter with only selected token
    * @param int $limit Max number of results
    *
    * @return array $result Delegations to selected account
    **/
    public function getDelegationTo(string $account, string $token = null, int $limit = 1): array
    {
        if ($token !== null) {
            $token = strtoupper($token);
        }

        $params = array(
            "contract" => "tokens",
            "table" => "delegations",
            "query" => array(
                "to" => $account,
                "symbol" => $token
            ),
            "limit" => $limit
        );

        $result = $this->HeLayer->call('find', $params);
        return $result;
    }

    /**
    * Get all delegations from selected account. Optional : only one token
    *
    * @param string $account Selected HIVE account
    * @param string $token Optional : filter with only selected token*
    * @param int $limit Max number of results
    *
    * @return array $result Delegations from selected account
    **/
    public function getDelegationFrom(string $account, string $token = null, int $limit = 1): array
    {
        if ($token !== null) {
            $token = strtoupper($token);
        }

        $params = array(
            "contract" => "tokens",
            "table" => "delegations",
            "query" => array(
                "from" => $account,
                "symbol" => $token
            ),
            "limit" => $limit
        );
        $result = $this->HeLayer->call('find', $params);
        return $result;
    }
}
