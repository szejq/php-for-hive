<?php

declare(strict_types=1);

namespace Hive\PhpLib;

/**
 * Hive transport layer
 *
 * Hive Toolbox PHP transport class
 * The HiveLayer class contains all methods needed to send queries
 * and receive response from HIVE blockchain.
 *
 * @category Layers
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class HiveLayer
{
    /**
     * If true, display the query & the result. Default: false
     *
     * @var bool $debug
     */
    private bool $debug = false;

    /**
     * Default node for query Hive API
     *
     * @var string $hiveNode
     */
    private string $hiveNode = "api.hive.blog";

    /**
     * If turned to true, curl disable SSL verification
     *
     * @var bool $disableSsl
     */
    private bool $disableSsl = false;

    /**
     * If true, throw Exception and stop execution
     *
     * @var bool $throwExceptions
     */
    private bool $throwExceptions = false;

    /**
     * Use HTTPS scheme by default
     *
     * @var string $scheme
     */
    private string $scheme = 'https://';

    /**
     * Constructor to use Hive and HiveEngine API with config
     * If $config is not present, use default values
     *
     * @param array<bool|string> $config Configuration array
     *
     * @return void
     */
    public function __construct(array $config = array())
    {

        if (array_key_exists('debug', $config)) {
            $this->debug = (bool) $config['debug'];
        }

        if (array_key_exists('hiveNode', $config)) {
            $this->hiveNode = (string) $config['hiveNode'];
        }

        if (array_key_exists('disableSsl', $config)) {
            $this->disableSsl = (bool) $config['disableSsl'];
        }

        if (array_key_exists('throwExceptions', $config)) {
            $this->throwExceptions = (bool) $config['throwExceptions'];
        }
    }

    /**
     * Function to generate JSON object for (RPC) request,
     * execute the query with curl(), and return the results
     *
     * @param string $method method
     * @param array  $params array of parameters
     *
     * @return array $response Decoded JSON from Hive response
     */
    public function call(string $method, array $params = array()): array
    {
        $request = $this->getRequest($method, $params);
        $json_response = $this->curl($request);

        $response = (array) json_decode($json_response, true);

        if (!array_key_exists('result', $response)) {
            if ($this->throwExceptions) {
                throw new \Exception('Error retrieve Hive API');
            }
            return $response;
        }

        return (array) $response['result'];
    }

    /**
     * Execute the cURL query and return the response JSON object
     *
     * @param string $data JSON object send to Hive selected node
     *
     * @return string $result result JSON object
     */
    public function curl(string $data): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->scheme . $this->hiveNode);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        if ($this->disableSsl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        $result = strval(curl_exec($ch));

        if ($this->debug) {
            echo "<pre><br>Result :<br>" . $result . "</pre>\n";
        }

        return $result;
    }

    /**
     * Function to generate JSON response for RPC HIVE & HE request from query
     *
     * @param string $method Method
     * @param array  $params Array of parameters
     *
     * @return string $response JSON object from Hive response
     */
    public function getRequest(string $method, array $params = array()): string
    {
        if (empty($params)) {
            $request = array(
                "jsonrpc" => "2.0",
                "method" => $method,
                "params" => [],
                "id" => 0
            );
        } else {
            $request = array(
                "jsonrpc" => "2.0",
                "method" => $method,
                "params" => $params,
                "id" => 0
            );
        }

        $request_json = json_encode($request);

        if ($this->debug) {
            echo "<pre><br>request_json<br/>";
            print $request_json . "\n";
            echo "</pre>";
        }
        return (string) $request_json;
    }
}
