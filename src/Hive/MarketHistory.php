<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Market History API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class MarketHistory
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'market_history_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Return internal market history (HBD:HIVE)
     *
     * @param int    $bucketSeconds number of seconds per bucket
     * @param string $start         stringified datetime 1970-01-01T00:00:00
     * @param string $end           stringified datetime 1970-01-01T00:00:00
     *
     * @return array $result Market history
     **/
    public function getMarketHistory(int $bucketSeconds, string $start, string $end): array
    {
        $params = [
            "bucket_seconds" => $bucketSeconds,
            "start" => $start,
            "end" => $end
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_market_history', $params);
        return $result;
    }

    /**
     * Return bucket seconds tracked by market history
     *
     * @return array $result Market history
     **/
    public function getMarketHistoryBuckets(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_market_history_buckets');
        return $result;
    }

    /**
     * Return internal market order book
     *
     * @param int $limit Limit number of results
     *
     * @return array $result Market order book
     **/
    public function getOrderBook(int $limit): array
    {
        $params = [
            "limit" => $limit
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_order_book', $params);
        return $result;
    }

    /**
     * Return recent trades
     *
     * @param int $limit Limit number of results
     *
     * @return array $result Market recent trades
     **/
    public function getRecentTrades(int $limit): array
    {
        $params = [
            "limit" => $limit
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_recent_trades', $params);
        return $result;
    }

    /**
     * Return internal market ticker
     *
     * @return array $result Market ticker
     **/
    public function getTicker(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_ticker');
        return $result;
    }

    /**
     * Return internal market trade history
     *
     * @param string $start stringified datetime 1970-01-01T00:00:00
     * @param string $end stringified datetime 1970-01-01T00:00:00
     * @param int $limit Limit number of results
     *
     * @return array $result Market trade history
     **/
    public function getTradeHistory(string $start, string $end, int $limit): array
    {
        $params = [
            "start" => $start,
            "end" => $end,
            "limit" => $limit
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_trade_history', $params);
        return $result;
    }

    /**
     * Return internal market 24h volume
     *
     * @return array $result Last 24h volume
     **/
    public function getVolume(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_volume');
        return $result;
    }
}
