<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Block API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class Block
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'block_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Return data from selected block
     *
     * @param int $block Block number
     *
     * @return array $result format : Data from the selected block
     **/
    public function getBlock(int $block): array
    {
        $params = ["block_num" => $block];
        $result = $this->HiveLayer->call($this->prefix . 'get_block', $params);
        return $result;
    }

    /**
     * Return header from selected block
     *
     * @param int $block Block number
     *
     * @return array $result format : Header from the selected block
     **/
    public function getBlockHeader(int $block): array
    {
        $params = ["block_num" => $block];
        $result = $this->HiveLayer->call($this->prefix . 'get_block_header', $params);
        return $result;
    }

    /**
     * Return range of blocks (full & signed)
     *
     * @param int $block Block number
     * * @param int $count Max number to return
     *
     * @return array $result format : Data from the in-range blocks
     **/
    public function getBlockRange(int $block, int $count): array
    {
        $params = [
            "starting_block_num" => $block,
            "count" => $count
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_block_range', $params);
        return $result;
    }
}
