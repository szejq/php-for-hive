<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * JSON-RPC API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class JsonRpc
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'jsonrpc.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Return All methods can be used by selected node
     *
     * @return array $result All node's methods
     **/
    public function getMethods(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_methods');
        return $result;
    }

    /**
     * Return sign information for JSON-RPC method
     *
     * @param string $method Name of the method
     *
     * @return array $result Returns arguments & response for the selected method
     **/
    public function getSignature(string $method): array
    {
        $params = ["method" => $method];
        $result = $this->HiveLayer->call($this->prefix . 'get_signature', $params);
        return $result;
    }
}
