<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Account by key API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class AccountByKey
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'account_by_key_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Used to lookup account information based on a key.
     *
     * @param string $author Author account
     *
     * @return array $result Detail from selected post
     **/
    public function getKeyReference(string $author): array
    {
        $params = [
            "author" => $author
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_key_references', $params);
        return $result;
    }
}
