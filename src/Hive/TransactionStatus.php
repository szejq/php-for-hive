<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Account by key API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class TransactionStatus
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'transaction_status_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Returns the status of a given transaction id.
     *
     * @param string $txid       ID of the selected transaction to find
     * @param string $expiration (optional) DateTime of the selected transaction.
     *
     * @return array $result Detail from selected post
     **/
    public function findTransaction(string $txid, string $expiration = null): array
    {
        $params = [
            "transaction_id" => $txid,
            "expiration" => $expiration
        ];
        $result = $this->HiveLayer->call($this->prefix . 'find_transaction', $params);
        return $result;
    }
}
