<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Reputation API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class Reputation
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'reputation_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Returns the reputation of selected accounts.
     *
     * @param string $lowerBound string to detect accounts starts with
     * @param int    $limit      (optional) Number of result (Default: 10)
     *
     * @return array $result Detail from selected post
     **/
    public function getAccountReputations(string $lowerBound, int $limit = 10): array
    {
        $params = [
            "account_lower_bound" => $lowerBound,
            "limit" => $limit
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_account_reputations', $params);
        return $result;
    }
}
