<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Condenser API (for production use)
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class Condenser
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'condenser_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Broadcast a trasaction
     *
     * @param int    $refBlockNum
     * @param int    $refBlockPrefix
     * @param string $expiration
     * @param array  $operations
     * @param array  $extensions
     * @param array  $signatures
     *
     * @return array $result
     **/
    public function broadcastTransaction(
        int $refBlockNum,
        int $refBlockPrefix,
        string $expiration,
        array $operations,
        array $extensions,
        array $signatures
    ): array {
        $params = [
            "ref_block_num" => $refBlockNum,
            "ref_block_prefix" => $refBlockPrefix,
            "expiration" => $expiration,
            "operations" => $operations,
            "extensions" => $extensions,
            "signatures" => $signatures
        ];
        $result = $this->HiveLayer->call($this->prefix . 'broadcast_transaction', $params);
        return $result;
    }

    /**
     * Return data from selected proposal
     *
     * @param int $proposal Proposal ID
     *
     * @return array $result Selected proposal data
     **/
    public function findProposal(int $proposal): array
    {
        $params = [[$proposal]];
        $result = $this->HiveLayer->call($this->prefix . 'find_proposals', $params);
        return $result;
    }

    /**
     * Return reccurent transfers of any liquid asset from selected account.
     *
     * @param string $account Selected account
     *
     * @return array $result State of selected node
     **/
    public function findReccurentTransfers(string $account): array
    {
        $params = [[$account]];
        $result = $this->HiveLayer->call($this->prefix . 'find_recurrent_transfers', $params);
        return $result;
    }

    /**
     * Return number of accounts.
     *
     * @return array $result Accounts number
     **/
    public function getAccountCount(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_account_count');
        return $result;
    }

    /**
     * Return Account history.
     * For set filters, please refer to :
     * https://gitlab.syncad.com/hive/hive/-/blob/master/libraries/protocol/include/hive/protocol/operations.hpp
     *
     * @param string $account Selected account
     * @param int $start Optional : Any positive numeric or -1 to reverse history
     * @param int $limit Optional : Max number of resutl (MAx: 1000)
     * @param int $opFilterLow Optional : bitmask
     * @param int $opFilterHigh Optional : bitmask
     *
     * @return array $result Accounts number
     **/
    public function getAccountHistory(
        string $account,
        int $start = -1,
        int $limit = 10,
        int $opFilterLow = null,
        int $opFilterHigh = null
    ): array {
        if ($opFilterLow === null) {
            $params = [$account, $start, $limit];
        } else {
            if ($opFilterHigh === null) {
                $params = [$account, $start, $limit, $opFilterLow];
            } else {
                $params = [$account, $start, $limit, $opFilterLow, $opFilterHigh];
            }
        }

        $result = $this->HiveLayer->call($this->prefix . 'get_account_history', $params);
        return $result;
    }

    /**
     * Return account reputations
     * If $limit >1, return accounts that start with $account string²
     *
     * @param string $account Selected account
     * @param int $limit Optional : Max number of results
     *
     * @return array $result Reputations of accounts
     **/
    public function getAccountReputations(string $account, int $limit = 100): array
    {
        $params = [$account, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_account_reputations', $params);
        return $result;
    }

    /**
     * Return selected account(s)
     *
     * @param string $account Selected account
     * @param bool $delayedVotes Optional : if false, hide account delayed votes
     *
     * @return array $result SAcocunt(s) data
     **/
    public function getAccounts(string $account, bool $delayedVotes = true): array
    {
        $params = [[$account], $delayedVotes];
        $result = $this->HiveLayer->call($this->prefix . 'get_accounts', $params);
        return $result;
    }

    /**
     * Return all votes on a post
     *
     * @param string $author Author account
     * @param string $permlink Slug of the post
     *
     * @return array $result votes from selected post
     **/
    public function getActiveVotes(string $author, string $permlink): array
    {
        $params = [$author, $permlink];
        $result = $this->HiveLayer->call($this->prefix . 'get_active_votes', $params);
        return $result;
    }

    /**
     * Return List of active witnesses
     *
     * @return array $result Active witnesses data
     **/
    public function getActiveWitnesses(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_active_witnesses');
        return $result;
    }

    /**
     * Return selected block details
     *
     * @param int $blockNum Selected block number
     *
     * @return array $result selected block details
     **/
    public function getBlock(int $blockNum): array
    {
        $params = [$blockNum];
        $result = $this->HiveLayer->call($this->prefix . 'get_block', $params);
        return $result;
    }

    /**
     * Return selected block header
     *
     * @param int $blockNum Selected block number
     *
     * @return array $result selected block header
     **/
    public function getBlockHeader(int $blockNum): array
    {
        $params = [$blockNum];
        $result = $this->HiveLayer->call($this->prefix . 'get_block_header', $params);
        return $result;
    }

    /**
     * Return blog posts from selected account
     *
     * @param string $account Selected account
     * @param int $start Optional : ID to start list
     * @param int $limit Optional : Max number of results
     *
     * @return array $result List of blog posts
     **/
    public function getBlog(string $account, int $start = 0, int $limit = 100): array
    {
        $params = [$account, $start, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_blog', $params);
        return $result;
    }

    /**
     * Return list of authors reblogged by selected account
     *
     * @param string $account Selected account
     *
     * @return array $result votes from selected post
     **/
    public function getBlogAuthor(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_blog', $params);
        return $result;
    }

    /**
     * Return blog entries for selected account
     *
     * @param string $account Selected account
     * @param int $start Optional : ID to start list
     * @param int $limit Optional : Max number of results
     *
     * @return array $result List of blog entries
     **/
    public function getBlogEntries(string $account, int $start = 0, int $limit = 100): array
    {
        $params = [$account, $start, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_blog_entries', $params);
        return $result;
    }

    /**
     * Return blockchain properties
     *
     * @return array $result Chain properties
     **/
    public function getChainProperties(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_chain_properties');
        return $result;
    }

    /**
     * Return discussions based on payout
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional
     *
     * @return array $result List of blog entries
     **/
    public function getCommentDiscussionsByPayout(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }

        $result = $this->HiveLayer->call($this->prefix . 'get_comment_discussions_by_payout', $params);
        return $result;
    }

    /**
     * Return config constants
     * More info : https://developers.hive.io/tutorials-recipes/understanding-configuration-values.html
     *
     * @return array $result Configuration values
     **/
    public function getConfig(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_config');
        return $result;
    }

    /**
     * Return content from selected post/conmment
     *
     * @param string $author Author account
     * @param string $permlink Slug of the post
     *
     * @return array $result Selected post/comment data
     **/
    public function getContent(string $author, string $permlink): array
    {
        $params = [$author, $permlink];
        $result = $this->HiveLayer->call($this->prefix . 'get_content', $params);
        return $result;
    }

    /**
     * Return selected post/conmment replies
     *
     * @param string $author Author account
     * @param string $permlink Slug of the post
     *
     * @return array $result Selected post/comment replies data
     **/
    public function getContentReplies(string $author, string $permlink): array
    {
        $params = [$author, $permlink];
        $result = $this->HiveLayer->call($this->prefix . 'get_content_replies', $params);
        return $result;
    }

    /**
     * Return selected ID conversion requests
     *
     * @param int $id ID of selected request
     *
     * @return array $result list of conversion requests
     **/
    public function getConversionRequests(int $id): array
    {
        $params = [$id];
        $result = $this->HiveLayer->call($this->prefix . 'get_conversion_requests', $params);
        return $result;
    }

    /**
     * Return Current median history prince
     *
     * @return array $result Base & quote prices
     **/
    public function getCurrentMedianHistoryPrice(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_current_median_history_price');
        return $result;
    }

    /**
     * Return discussions based on active
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByActive(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_active', $params);
        return $result;
    }

    /**
     * Return author's discussions before data
     *
     * @param string $author Author account
     * @param string $permlink Slug of the post
     * @param string $datetime Stringified datetime 1970-01-01T00:00:00
     * @param int $limit Optional Max number of results
     *
     * @return array $result Selected post discussion before date
     **/
    public function getDiscussionsByAuthorBeforeDate(
        string $author,
        string $permlink,
        string $datetime,
        int $limit = 10
    ): array {
        $params = [$author, $permlink, $datetime, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_author_before_date', $params);
        return $result;
    }

    /**
     * Return discussions by blog
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByBlog(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_blog', $params);
        return $result;
    }

    /**
     * Return discussions by cashout
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByCashout(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_cashout', $params);
        return $result;
    }

    /**
     * Return discussions by children
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByChildren(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_children', $params);
        return $result;
    }

    /**
     * Return discussions by comments
     *
     * @param string $startAuthor Author account
     * @param string $startPermlink Slug of the post
     * @param int $limit Optional Max number of results
     *
     * @return array $result Selected discussions comments
     **/
    public function getDiscussionsByComments(string $startAuthor, string $startPermlink, int $limit = 1): array
    {
        $params = [
            "start_author" => $startAuthor,
            "start_permlink" => $startPermlink,
            "limit" => $limit
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_comments', $params);
        return $result;
    }

    /**
     * Return discussions by created
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByCreated(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_created', $params);
        return $result;
    }

    /**
     * Return discussions by feed
     *
     * @param string $tag selected tag filter
     * @param string $startAuthor Optional: Author account
     * @param string $startPermlink Optional: Slug of post
     * @param int $limit Optional: Max number of results
     *
     * @return array $result Selected discussions feed
     **/
    public function getDiscussionsByFeed(string $tag, string $startAuthor, string $startPermlink, int $limit = 1): array
    {
        $params = [
            "tag" => $tag,
            "limit" => $limit
        ];
        if (!empty($startAuthor)) {
            $params["start_author"] = $startAuthor;
        }
        if (!empty($startPermlink)) {
            $params["start_permlink"] = $startPermlink;
        }

        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_feed', $params);
        return $result;
    }

    /**
     * Return discussions by hot
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional if true, remove body (default : false)
     *Hot
     * @return array $result List of discussions
     **/
    public function getDiscussionsByHot(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_hot', $params);
        return $result;
    }

    /**
     * Return discussions by promoted
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional if true, remove body (default : false)
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByPromoted(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_promoted', $params);
        return $result;
    }

    /**
     * Return discussions by trending
     *
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional if true, remove body (default : false)
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByTrending(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_trending', $params);
        return $result;
    }

    /**
     * Return discussions by votes
     *trending
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional if true, remove body (default : false)
     *
     * @return array $result List of discussions
     **/
    public function getDiscussionsByVotes(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_discussions_by_votes', $params);
        return $result;
    }

    /**
     * Return global properties
     * More info : https://developers.hive.io/tutorials-recipes/understanding-dynamic-global-properties.html
     *
     * @return array $result Configuration values
     **/
    public function getDynamicGlobalProperties(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_dynamic_global_properties');
        return $result;
    }

    /**
     * Return escrow for a certain account by id.
     *
     * @param string $account selected account
     * @param int $id escrow ID
     *
     * @return array $result
     **/
    public function getEscrow(string $account, int $id): array
    {
        $params = [$account, $id];
        $result = $this->HiveLayer->call($this->prefix . 'get_content_replies', $params);
        return $result;
    }

    /**
     * Return expiring vesting delegations for selected account
     *
     * @param string $account selected account
     * @param string $after Stringified datetime 2018-01-01T00:00:00
     *
     * @return array $result
     **/
    public function getExpiringVestingDelegations(string $account, string $after): array
    {
        $params = [$account, $after];
        $result = $this->HiveLayer->call($this->prefix . 'get_expiring_vesting_delegations', $params);
        return $result;
    }

    /**
     * Return items in selected account feed
     *
     * @param string $account selected account
     * @param int $start Start entry ID (Default : 0)
     * @param int $limit Max number of results (Default : 10)
     *
     * @return array $result List of items
     **/
    public function getFeed(string $account, int $start = 0, int $limit = 10): array
    {
        $params = [$account, $start, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_feed', $params);
        return $result;
    }

    /**
     * Return entries in selected account feed
     *
     * @param string $account selected account
     * @param int $start Start entry ID (Default : 0)
     * @param int $limit Max number of results (Default : 10)
     *
     * @return array $result List of entries
     **/
    public function getFeedEntries(string $account, int $start = 0, int $limit = 10): array
    {
        $params = [$account, $start, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_feed_entries', $params);
        return $result;
    }

    /**
     * Return price feed values history
     *
     * @return array $result Price feed history
     **/
    public function getFeedHistory(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_feed_history');
        return $result;
    }

    /**
     * Return followers & following count of selected user
     *
     * @param string $account Selected account
     *
     * @return array $result Followers & following count
     **/
    public function getFollowCount(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_follow_count', $params);
        return $result;
    }

    /**
     * Return followers list of selected user
     *
     * @param string $account Selected account
     * @param string $start Account to start from. Default: null
     * @param string $type "blog" (non-muted users) or "ingore" (muted users). Default : "blog"
     * @param int $limit Max number of results (Default : 1000)
     *
     * @return array $result Followers list
     **/
    public function getFollowers(string $account, string $start = null, string $type = "blog", int $limit = 1000): array
    {
        $params = [$account, $start, $type, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_followers', $params);
        return $result;
    }

    /**
     * Return following list of selected user
     *
     * @param string $account Selected account
     * @param string $start Account to start from. Default: null
     * @param string $type "blog" (non-muted users) or "ingore" (muted users). Default : "blog"
     * @param int $limit Max number of results (Default : 1000)
     *
     * @return array $result Following list
     **/
    public function getFollowing(string $account, string $start = null, string $type = "blog", int $limit = 1000): array
    {
        $params = [$account, $start, $type, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_following', $params);
        return $result;
    }

    /**
     * Return HF version
     *
     * @return array $result HarfFork version
     **/
    public function getHardforkVersion(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_hardfork_version');
        return $result;
    }

    /**
     * Return accounts with associated key
     *
     * @param string $key Key
     *
     * @return array $result list of accounts asdsociated with selected key
     **/
    public function getKeyReferences(string $key): array
    {
        $params = [[$key]];
        $result = $this->HiveLayer->call($this->prefix . 'get_key_references', $params);
        return $result;
    }

    /**
     * Return next scheduled HF
     *
     * @return array $result date & version of HF
     **/
    public function getNextScheduledHardfork(): array
    {
        $result = $this->HiveLayer->call($this->prefix . '.get_next_scheduled_hardfork');
        return $result;
    }

    /**
     * Return open Orders from selected account
     *
     * @param string $account Selected account
     *
     * @return array $result open orders list
     **/
    public function getOpenOrders(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_open_orders', $params);
        return $result;
    }

    /**
     * Return operations in sleected block
     *
     * @param int $blockNum ID of selected block
     * @param bool $onlyVirtual If true, display only virtual ops
     *
     * @return array $result list of all operations in selected block
     **/
    public function getOpsInBlock(int $blockNum, bool $onlyVirtual = false): array
    {
        $params = [$blockNum, $onlyVirtual];
        $result = $this->HiveLayer->call($this->prefix . 'get_ops_in_block', $params);
        return $result;
    }

    /**
     * Return owner history of selected account
     *
     * @param string $account Selected account
     *
     * @return array $result Owner history
     **/
    public function getOwnerHistory(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_owner_history', $params);
        return $result;
    }

    /**
     * Return post discussions by payout
     *trending
     * @param string $tag Selected tag
     * @param int $limit Optional : Max number of results
     * @param array $filterTags Optional
     * @param array $selectAuthors Optional
     * @param array $selectTags Optional
     * @param bool $truncateBody Optional if true, remove body (default : false)
     *
     * @return array $result List of post discussions
     **/
    public function getPostDiscussionsByPayout(
        string $tag,
        int $limit = 100,
        array $filterTags = [],
        array $selectAuthors = [],
        array $selectTags = [],
        bool $truncateBody = false
    ): array {
        $params = [
            "tag" => $tag,
            "limit" => $limit,
            "truncate_body" => $truncateBody
        ];
        if (!empty($filterTags)) {
            $params["filter_tags"] = $filterTags;
        }
        if (!empty($selectAuthors)) {
            $params["select_authors"] = $selectAuthors;
        }
        if (!empty($selectTags)) {
            $params["select_tags"] = $selectTags;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_post_discussions_by_payout', $params);
        return $result;
    }

    /**
     * Return set of all public keys that could possibly sign for a given transaction
     *
     * @param int $refBlockNum Block number reference
     * @param int $refBlockPrefix Block prefix reference
     * @param string $expiration Stringify Datetime 1970-01-01T00:00:00
     * @param array $operations Optional
     * @param array $extensions Optional
     * @param array $signatures Optional
     *
     * @return array $result List of discussions
     **/
    public function getPotentialSignatures(
        int $refBlockNum,
        int $refBlockPrefix,
        string $expiration,
        array $operations = [],
        array $extensions = [],
        array $signatures = []
    ): array {
        $params = [
            "refBlockNum" => $refBlockNum,
            "refBlockPrefix" => $refBlockPrefix,
            "expiration" => $expiration
        ];
        if (!empty($operations)) {
            $params["operations"] = $operations;
        }
        if (!empty($extensions)) {
            $params["extensions"] = $extensions;
        }
        if (!empty($signatures)) {
            $params["signatures"] = $signatures;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_potential_signatures', $params);
        return $result;
    }

    /**
     * Return list of authors that rebloged the selected post
     *
     * @param string $author Author account
     * @param string $permlink Slug of the post
     *
     * @return array $result list authors
     **/
    public function getRebloggedBy(string $author, string $permlink): array
    {
        $params = [$author, $permlink];
        $result = $this->HiveLayer->call($this->prefix . 'get_reblogged_by', $params);
        return $result;
    }

    /**
     * Return recovery request from selected account
     *
     * @param string $account Selected account
     *
     * @return array $result list authors
     **/
    public function getRecoveryRequest(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_recovery_request', $params);
        return $result;
    }

    /**
     * Return list of replies by last update
     *
     * @param string $author Author account
     * @param string $permlink Slug of the post
     * @param int $limit Max number of results
     *
     * @return array $result List of replies
     **/
    public function getRepliesByLastUpdate(string $author, string $permlink, int $limit = 100): array
    {
        $params = [$author, $permlink, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_replies_by_last_update', $params);
        return $result;
    }

    /**
     * Returnminimal subset of public keys that should add signatures to the transaction
     *
     * @param int $refBlockNum Block number reference
     * @param int $refBlockPrefix Block prefix reference
     * @param string $expiration Stringify Datetime 1970-01-01T00:00:00
     * @param array $operations Optional
     * @param array $extensions Optional
     * @param array $signatures Optional
     *
     * @return array $result List of discussions
     **/
    public function getRequiredSignatures(
        int $refBlockNum,
        int $refBlockPrefix,
        string $expiration,
        array $operations = [],
        array $extensions = [],
        array $signatures = []
    ): array {
        $params = [
            "refBlockNum" => $refBlockNum,
            "refBlockPrefix" => $refBlockPrefix,
            "expiration" => $expiration
        ];
        if (!empty($operations)) {
            $params["operations"] = $operations;
        }
        if (!empty($extensions)) {
            $params["extensions"] = $extensions;
        }
        if (!empty($signatures)) {
            $params["signatures"] = $signatures;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_required_signatures', $params);
        return $result;
    }

    /**
     * Return data about reward fund
     *
     * @return array $result List of replies
     **/
    public function getRewardFund(): array
    {
        $params = ["post"];
        $result = $this->HiveLayer->call($this->prefix . 'get_reward_fund', $params);
        return $result;
    }

    /**
     * Return savings withdraw from selected account
     *
     * @param string $account Selected account
     *
     * @return array $result savings withdraw
     **/
    public function getSavingsWithdrawFrom(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_savings_withdraw_from', $params);
        return $result;
    }

    /**
     * Return savings withdraw to selected account
     *
     * @param string $account Selected account
     *
     * @return array $result savings withdraw
     **/
    public function getSavingsWithdrawTo(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_savings_withdraw_to', $params);
        return $result;
    }

    /**
     * Return List of tags used by selected account
     *
     * @param string $account Selected account
     *
     * @return array $result List of tags
     **/
    public function getTagsUsedByAuthor(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_tags_used_by_author', $params);
        return $result;
    }

    /**
     * Return internal HBD/HIVE market ticker
     *
     * @return array $result Market ticker
     **/
    public function getTicker(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_ticker');
        return $result;
    }

    /**
     * Return selected transaction details
     *
     * @param string $tx ID of the selected transaction
     *
     * @return array $result List of tags
     **/
    public function getTransaction(string $tx): array
    {
        $params = [$tx];
        $result = $this->HiveLayer->call($this->prefix . 'get_transaction', $params);
        return $result;
    }

    /**
     * Return hex of serialized binary form of tx
     *
     * @param int $refBlockNum Block number reference
     * @param int $refBlockPrefix Block prefix reference
     * @param string $expiration Stringify Datetime 1970-01-01T00:00:00
     * @param array $operations Optional
     * @param array $extensions Optional
     * @param array $signatures Optional
     *
     * @return array $result hexdump
     **/
    public function getTransactionHex(
        int $refBlockNum,
        int $refBlockPrefix,
        string $expiration,
        array $operations = [],
        array $extensions = [],
        array $signatures = []
    ): array {
        $params = [
            "refBlockNum" => $refBlockNum,
            "refBlockPrefix" => $refBlockPrefix,
            "expiration" => $expiration
        ];
        if (!empty($operations)) {
            $params["operations"] = $operations;
        }
        if (!empty($extensions)) {
            $params["extensions"] = $extensions;
        }
        if (!empty($signatures)) {
            $params["signatures"] = $signatures;
        }
        $result = $this->HiveLayer->call($this->prefix . 'get_transaction_hex', $params);
        return $result;
    }

    /**
     * Return list of trending tags
     *
     * @param string $tag Optional Selected tag
     * @param int $limit Optional Max Number of results. Max: 100
     *
     * @return array $result List of tags
     **/
    public function getTrendingTags(string $tag = null, int $limit = 100): array
    {
        $params = [$tag, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_trending_tags', $params);
        return $result;
    }

    /**
     * Return the actual version of blockchain
     *
     * @return array $result blockchain version, HIVE revision, and FC revision
     **/
    public function getVersion(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_version');
        return $result;
    }

    /**
     * Return vesting delegations by selected account
     *
     * @param string $delegAccount Selected account
     * @param string $startAccount Optional: start from this account
     * @param int $limit Optional Max Number of results. Max: 1000
     *
     * @return array $result List of vesting delegations
     **/
    public function getVestingDelegations(string $delegAccount, string $startAccount, int $limit = 100): array
    {
        $params = [$delegAccount, $startAccount, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_vesting_delegations', $params);
        return $result;
    }

    /**
     * Return withdraw routes for selected account
     *
     * @param string $account Selected account
     * @param string $type "outgoing", "incomping" or "all"
     *
     * @return array $result List of withdraw routes
     **/
    public function getWithdrawRoutes(string $account, string $type): array
    {
        $params = [$account, $type];
        $result = $this->HiveLayer->call($this->prefix . 'get_withdraw_routes', $params);
        return $result;
    }

    /**
     * Return witness of selected account
     *
     * @param string $account Selected account
     *
     * @return array $result witness
     **/
    public function getWitnessByAccount(string $account): array
    {
        $params = [$account];
        $result = $this->HiveLayer->call($this->prefix . 'get_witness_by_account', $params);
        return $result;
    }

    /**
     * Return witness count
     *
     * @return array $result witness count
     **/
    public function getWitnessCount(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_witness_count', []);
        return $result;
    }

    /**
     * Return current witness schedule
     *
     * @return array $result witness shedule
     **/
    public function getWitnessSchedule(): array
    {
        $result = $this->HiveLayer->call($this->prefix . 'get_witness_schedule', []);
        return $result;
    }

    /**
     * Return selected witness
     *
     * @param array $ids IDs of selected witnesses (example : [1, 5, 24])
     *
     * @return array $result Selected witnesses details
     **/
    public function getWitnesses(array $ids): array
    {
        $params = [$ids];
        $result = $this->HiveLayer->call($this->prefix . 'get_witnesses', $params);
        return $result;
    }

    /**
     * Return current Witnesses by vote
     *
     * @param string $account Selected account. Default: null (to get top witnesses)
     * @param int $limit Max number of results. Default: 100
     *
     * @return array $result List of withdraw routes
     **/
    public function getWitnessesByVote(string $account = null, int $limit = 100): array
    {
        $params = [$account, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'get_witnesses_by_vote', $params);
        return $result;
    }

    /**
     * Return true if transaction is not expired or been invalided
     *
     * @param string $txid Transaction ID
     *
     * @return array $result Selected witnesses details
     **/
    public function isKnownTransaction(string $txid): array
    {
        $params = [$txid];
        $result = $this->HiveLayer->call($this->prefix . 'is_known_transaction', $params);
        return $result;
    }

    /**
     * Return Looks up accounts starting with name
     *
     * @param string $lowerBound string to search accounts starting with
     * @param int $limit Max number of results
     *
     * @return array $result list of accounts
     **/
    public function lookupAccounts(string $lowerBound, int $limit = 100): array
    {
        $params = [$lowerBound, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'lookup_accounts', $params);
        return $result;
    }

    /**
     * Return followers & following count of selected user
     *
     * @param string $account Selected account
     * @param bool $delayedVotes if true, display delayed votes
     *
     * @return array $result Followers & following count
     **/
    public function lookupAccountNames(string $account, bool $delayedVotes = true): array
    {
        $params = [[$account, $delayedVotes]];
        $result = $this->HiveLayer->call($this->prefix . 'lookup_account_names', $params);
        return $result;
    }

    /**
     * Return Looks up witness accounts starting with name
     *
     * @param string $lowerBound string to search accounts starting with
     * @param int $limit Max number of results
     *
     * @return array $result list of accounts
     **/
    public function lookupWitnessAccounts(string $lowerBound, int $limit = 100): array
    {
        $params = [$lowerBound, $limit];
        $result = $this->HiveLayer->call($this->prefix . 'lookup_witness_accounts', $params);
        return $result;
    }
}
