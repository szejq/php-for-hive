<?php

declare(strict_types=1);

namespace Hive\PhpLib\Hive;

use Hive\PhpLib\HiveLayer as HiveLayer;

/**
 * Tags API
 *
 * @category Methods
 * @package  Hive
 * @author   Florent Bambukah Kosmala <contact@florent-kosmala.fr>
 * @license  https://www.opensource.org/licenses/mit-license.html MIT License
 */
class Tags
{
    /**
     * Needed layer for Hive communication
     */
    private object $HiveLayer;

    /**
     * Prefix for which API you use
     *
     * @var string $prefix
     */
    private $prefix = 'tags_api.';

    /**
     * Constructor to apply the config array
     *
     * @param array $config Configuration Array
     *
     * @return void
     */
    public function __construct(array $config)
    {
        $this->HiveLayer = new HiveLayer($config);
    }

    /**
     * Return all details from one post.
     *
     * @param string $author   Author account
     * @param string $permlink Permanent link to the post
     *
     * @return array $result Detail from selected post
     **/
    public function getDiscussion(string $author, string $permlink): array
    {
        $params = [
            "author" => $author,
            "permlink" => $permlink
        ];
        $result = $this->HiveLayer->call($this->prefix . 'get_discussion', $params);
        return $result;
    }
}
