<?php

/*
 * Load composer dependancies
 * In your project, don't forget to check if it's the good path.
 */
require __DIR__ . '/../../vendor/autoload.php';

/*
 * Declase use statement to add Hive Condenser lib
 */
use Hive\PhpLib\Hive\Condenser as HiveCondenser;

/*
 *  Create config array with settings
 */
$config = [
    "debug" => false,
    "heNode" => "api.hive-engine.com/rpc",
    "hiveNode" => "anyx.io"
];

/*
 * fill the needed vars for this example
 */
$lowerBound = 'bamb';
$limit = 10;
/*
 * Let's Go! Load the Condenser API, execture the findProposal() method
 * and display info.
 */
$api = new HiveCondenser($config);
$result = $api->lookupAccounts($lowerBound, $limit);
print_r($result);
